package at.resch.xml;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Gag {
	
	private String heading;
	private LocalDateTime dateTime;
	private String link;
	
	private static final DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy kk:mm:ss");
	
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public LocalDateTime getDateTime() {
		return dateTime;
	}
	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	@Override
	public String toString() {
		return "Gag [heading=" + heading + ", dateTime=" + dateTime.format(format) + ", link="
				+ link + "]";
	}
	
	

}
