package at.resch.xml;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public class GagReader {

	public List<Gag> read () throws IOException {
		return read(new URL("http://9gag-rss.com/api/rss/get?code=9GAG&format=2").openStream());
	}

	public List<Gag> read(InputStream in) throws IOException {
		final String ITEM = "item";
		List<Gag> items = new ArrayList<>();

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss").withZone(ZoneId.of("UTC"));
		
		try {
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			Gag item = null;
			while (eventReader.hasNext()) {
				XMLEvent ev = eventReader.nextEvent();
				if (ev.isStartElement()) {
					switch (ev.asStartElement().getName().getLocalPart()) {
					case ITEM:
						item = new Gag();
						break;
					case "title":
						if(item != null) {
							ev = eventReader.nextEvent();
							item.setHeading(ev.asCharacters().getData());
						}
						continue;
					case "link":
						if(item != null) {
							ev = eventReader.nextEvent();
							item.setLink(ev.asCharacters().getData());
						}
						continue;
					case "updated" :
						if(item != null) {
							ev = eventReader.nextEvent();
							item.setDateTime(LocalDateTime.parse(ev.asCharacters().getData().subSequence(0, 19), format));
						}
						continue;
					}
				}
				if (ev.isEndElement()) {
					if (ev.asEndElement().getName().getLocalPart()
							.equals(ITEM)) {
						items.add(item);
					}

				}
			}
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}

		return items;
	}
	
}
